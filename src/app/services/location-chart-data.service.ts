import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {environment} from '../../environments/environment';
import {WeekAvg} from '../models/WeekAvg';
import {MonthAvg} from '../models/MonthAvg';
import {WaterLevel} from '../models/water-level';


@Injectable({
  providedIn: 'root'
})
export class LocationChartDataService {

  private weekChartData: WeekAvg;
  public weekChartDataChange: Subject<WeekAvg> = new Subject<WeekAvg>();

  private monthChartAvg: MonthAvg;
  public monthChartAvgDataChartChange: Subject<MonthAvg> = new Subject<MonthAvg>();

  private twentyFourHoursData: WaterLevel[];
  public twentyFourHoursChanged: Subject<WaterLevel[]> = new Subject<WaterLevel[]>();

  constructor(private http: HttpClient) {
  }

  // region MonthAvg
  public getMonthChartAvg(id: number): MonthAvg {
    this.fetchMonthChartAvg(id);
    return this.monthChartAvg;
  }

  private fetchMonthChartAvg(id: number): void {
    this.http.get<MonthAvg>(
      `${environment.apiUrl}/v1/waterlevels/avgAllMonth?location_id=${id}&year=${(new Date()).getFullYear()}`
    ).subscribe(weekChartData => {
      this.setMonthChartAvg(weekChartData);
    });
  }

  public setMonthChartAvg(weekChartData: MonthAvg): void {
    this.monthChartAvg = weekChartData;
    this.monthChartAvgDataChartChange.next(this.monthChartAvg);
  }

  //endregion

  // region WeekAvg
  public getWeekChartData(id: number): WeekAvg {
    this.fetchWeekChartData(id);
    return this.weekChartData;
  }

  private fetchWeekChartData(id: number): void {
    this.http.get<WeekAvg>(`${environment.apiUrl}/v1/WeekChartData/${id}`).subscribe(weekChartData => {
      this.setWeekChartData(weekChartData);
    });
  }

  public setWeekChartData(weekChartData: WeekAvg): void {
    this.weekChartData = weekChartData;
    this.weekChartDataChange.next(this.weekChartData);
  }

  //endregion

  // region 24 hours
  public get24HoursChart(id: number): WaterLevel[] {
    this.fetchWeekChartData(id);
    return this.twentyFourHoursData;
  }

  private fetch24HoursChart(id: number): void {
    this.http.get<WaterLevel[]>(`${environment.apiUrl}/v1/waterlevels/avgLastTwentyFourHoursPerHour/${id}`).subscribe(wl => {
      this.set24HoursChart(wl);
    });
  }

  public set24HoursChart(data: WaterLevel[]): void {
    this.twentyFourHoursData = data;
    this.twentyFourHoursChanged.next(this.twentyFourHoursData);
  }

  //endregion

}
