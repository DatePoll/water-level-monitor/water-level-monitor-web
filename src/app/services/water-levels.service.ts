import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {environment} from '../../environments/environment';
import {WaterLevel} from '../models/water-level';

@Injectable({
  providedIn: 'root'
})
export class WaterLevelsService {
  private waterLevelsTwentyFourHours: WaterLevel[] = [];
  public waterLevelsTwentyFourHoursChange: Subject<WaterLevel[]> = new Subject<WaterLevel[]>();

  constructor(private http: HttpClient) {
  }

  public getWaterLevelsTwentyFourHours(locationId: number): WaterLevel[] {
    this.fetchWaterLevelsTwentyFourHours(locationId);
    return this.waterLevelsTwentyFourHours.slice();
  }

  private fetchWaterLevelsTwentyFourHours(locationId: number): void {
    this.http.get<WaterLevel[]>(`${environment.apiUrl}/v1/waterlevels/avgLastTwentyFourHoursPerHour/` + locationId).subscribe(wl => {
      this.setWaterLevelsTwentyFourHours(wl);
    });
  }

  public setWaterLevelsTwentyFourHours(waterLevels: WaterLevel[]): void {
    this.waterLevelsTwentyFourHours = waterLevels;
    this.waterLevelsTwentyFourHoursChange.next(this.waterLevelsTwentyFourHours.slice());
  }
}
