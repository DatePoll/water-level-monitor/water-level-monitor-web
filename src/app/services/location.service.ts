import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {Location} from '../models/Location';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private locations: Location[] = [];
  public locationsChange: Subject<Location[]> = new Subject<Location[]>();

  constructor(private http: HttpClient) {
  }

  public getLocations(id: number): Location[] {
    this.fetchLocations(id);
    return this.locations.slice();
  }

  private fetchLocations(id: number): void {
    this.http.get<Location[]>(`${environment.apiUrl}/v1/locations/${id}`).subscribe(locations => {
      this.setLocations(locations);
    });
  }

  public setLocations(location: Location[]): void{
    this.locations = location;
    this.locationsChange.next(this.locations.slice());
  }
}
