import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {River} from '../models/River';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RiverService {
  private rivers: River[] = [];
  public riversChange: Subject<River[]> = new Subject<River[]>();

  constructor(private http: HttpClient) {
  }

  public getRivers(): River[] {
    this.fetchRivers();
    return this.rivers.slice();
  }

  private fetchRivers(): void {
    this.http.get<River[]>(`${environment.apiUrl}/v1/rivers`).subscribe(rivers => {
      this.setRivers(rivers);
    });
  }

  public setRivers(rivers: River[]): void {
    this.rivers = rivers;
    this.riversChange.next(this.rivers.slice());
  }
}
