import {AfterViewInit, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit, AfterViewInit {

  @Input() name: string;
  @Input() chartLabels: string[];
  @Input() data: number[];

  public chartData: object[];
  public chartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    beginAtZero: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };
  public chartType = 'line';
  public chartLegend = true;

  constructor() {
  }

  ngOnInit(): void {
    this.chartData = [
      {
        data: this.data,
        label: this.name,
        backgroundColor: '#a1c5ff',
        borderColor: '#7698cf',
        pointBackgroundColor: '#a1c5ff',
        lineTension: '0'
      },
    ];
  }

  ngAfterViewInit(): void {
    this.chartData = [
      {
        data: this.data,
        label: this.name,
        backgroundColor: '#a1c5ff',
        borderColor: '#7698cf',
        pointBackgroundColor: '#a1c5ff',
        lineTension: '0'
      },
    ];
  }
}
