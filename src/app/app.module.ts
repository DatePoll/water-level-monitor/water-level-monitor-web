import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ChartsModule} from 'ng2-charts';
import {LineChartComponent} from './line-chart/line-chart.component';
import {HttpClientModule} from '@angular/common/http';
import {MainComponent} from './main/main.component';
import {routing} from './app-routing.module';
import { RiverDetailsComponent } from './river-details/river-details.component';
import { LocationDetailsComponent } from './river-details/location-details/location-details.component';
import {RouterModule} from '@angular/router';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    LineChartComponent,
    MainComponent,
    RiverDetailsComponent,
    LocationDetailsComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    routing,
    ChartsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
