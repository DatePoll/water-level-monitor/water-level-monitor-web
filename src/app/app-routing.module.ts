import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main/main.component';
import {RiverDetailsComponent} from './river-details/river-details.component';
import {LineChartComponent} from './line-chart/line-chart.component';
import {locationRoutes} from './river-details/locations.routes';

const mainRoutes: Routes = [
  { path: '', component: MainComponent,  },
  { path: 'rivers/:id', component: RiverDetailsComponent, children: locationRoutes},
  { path: 'linechart', component: LineChartComponent}
];

export const routing = RouterModule.forRoot(mainRoutes, { relativeLinkResolution: 'legacy' });

