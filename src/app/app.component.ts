import {Component, OnDestroy} from '@angular/core';
import {River} from './models/River';
import {Subscription} from 'rxjs';
import {RiverService} from './services/river.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  title = 'Water level monitor';

  public rivers: River[];
  public riversSubscription: Subscription;

  constructor(private riverService: RiverService) {
    this.rivers = this.riverService.getRivers();
    this.riversSubscription = this.riverService.riversChange.subscribe(value => {
        this.rivers = value;
      }
    );
  }

  ngOnDestroy(): void {
    this.riversSubscription.unsubscribe();
  }
}
