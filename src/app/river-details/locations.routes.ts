import {Routes} from '@angular/router';
import {LocationDetailsComponent} from './location-details/location-details.component';

export const locationRoutes: Routes = [
  { path: 'locations/:lid', component: LocationDetailsComponent }
];


