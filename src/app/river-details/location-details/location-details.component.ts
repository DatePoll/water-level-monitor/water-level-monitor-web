import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LocationChartDataService} from '../../services/location-chart-data.service';
import {Subscription} from 'rxjs';
import {MonthAvg} from '../../models/MonthAvg';
import {WaterLevel} from '../../models/water-level';
import {WaterLevelsService} from '../../services/water-levels.service';
import {DatePipe, formatDate} from '@angular/common';

@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.component.html',
  styleUrls: ['./location-details.component.scss']
})
export class LocationDetailsComponent implements OnDestroy {

  public locationChartDataMonth: MonthAvg;
  public locationChartDataMonthSubscription: Subscription;
  public monthData: number[];
  public monthVisible = false;

  public twentyFourHoursWL: WaterLevel[];
  public twentyFourHoursWLDataSubscription: Subscription;
  public twentyFourHoursWLData: number[];
  public twentyFourHoursLabel: string[];
  public twentyFourVisible = false;

  public getLocationChartDataMonthArray(): number[] {
    const res = this.locationChartDataMonth;
    return new MonthAvg(res).formatCorrect();
  }

  constructor(
    private locationChartService: LocationChartDataService,
    private waterLevelService: WaterLevelsService,
    private activatedRoute: ActivatedRoute) {

    this.activatedRoute.params.subscribe(params => {
      const riverId = +params.id;
      const locationId = +params.lid;

      this.setup24HoursChart(locationId);
      this.setup12MonthChart(locationId);
    });
  }

  private setup12MonthChart(locationId: number): void {
    this.monthData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    this.locationChartDataMonthSubscription = this.locationChartService.monthChartAvgDataChartChange.subscribe(value => {
      if (value !== null) {
        this.locationChartDataMonth = value;
        this.monthData = this.getLocationChartDataMonthArray();
        this.monthVisible = true;
      }
    });

    this.locationChartService.getMonthChartAvg(locationId);
  }

  private setup24HoursChart(locationId: number): void {
    this.twentyFourHoursWLData = [];
    this.twentyFourHoursLabel = [];

    this.twentyFourHoursWLDataSubscription = this.waterLevelService.waterLevelsTwentyFourHoursChange.subscribe(value => {
      if (value !== null) {
        this.formatUpdate24Date(value.reverse());
      }
    });

    this.waterLevelService.getWaterLevelsTwentyFourHours(locationId);
  }

  private formatUpdate24Date(value: any): void {
    this.twentyFourHoursWL = value;
    this.twentyFourHoursWLData = [];
    const pipe = new DatePipe('de-DE');
    let counter = 0;
    this.twentyFourHoursWL.forEach(wl => {
      this.twentyFourHoursWLData[counter] = wl.level;
      let date: Date;
      if (wl.date.includes('z') || wl.date.includes('Z')) {
        date = new Date(wl.date);
      } else {
        date = new Date(wl.date + 'Z');
      }

      this.twentyFourHoursLabel[counter] = formatDate(date, 'HH:mm', 'en-us', '');
      counter++;
    });

    this.twentyFourVisible = true;
  }

  ngOnDestroy(): void {
    this.locationChartDataMonthSubscription.unsubscribe();
    this.twentyFourHoursWLDataSubscription.unsubscribe();
  }
}
