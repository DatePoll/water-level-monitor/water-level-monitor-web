import {AfterViewChecked, AfterViewInit, Component, DoCheck, Input, OnChanges, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {LocationService} from '../services/location.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '../models/Location';
import {River} from '../models/River';
import {RiverService} from '../services/river.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-river-details',
  templateUrl: './river-details.component.html',
  styleUrls: ['./river-details.component.scss']
})
export class RiverDetailsComponent implements OnDestroy, AfterViewChecked{

  public locations: Location[] = [];
  public locationSubscription: Subscription;

  public river: River;
  public riverSubscription: Subscription;

  constructor(
    private locationService: LocationService,
    private riverService: RiverService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.activatedRoute.params.subscribe(
      params => {

        this.locationSubscription = this.locationService.locationsChange.subscribe(value => {
            this.locations = value;
            const str = '/rivers/' + this.river.id + '/locations/' + this.locations[0].id;
            this.router.navigateByUrl(str);
          }
        );
        this.locations = this.locationService.getLocations(+params.id);

        this.riverSubscription = this.riverService.riversChange.subscribe(value => {
          this.river = value.find(p => p.id === +params.id);
        });
        this.river = riverService.getRivers().find(p => p.id === +params.id);
      }
    );
  }

  ngAfterViewChecked(): void {
    if (!this.router.url.includes('location')) {
      if (this.locations[0] !== undefined) {
        const str = '/rivers/' + this.river.id + '/locations/' + this.locations[0].id;
        this.router.navigateByUrl(str);
      }
    }
  }

  ngOnDestroy(): void {
    this.locationSubscription.unsubscribe();
    this.riverSubscription.unsubscribe();
  }
}
