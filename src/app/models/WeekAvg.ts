export class WeekAvg {
  public JAN: number;
  public FEB: number;
  public MAR: number;
  public APR: number;
  public MAY: number;
  public JUN: number;
  public JUL: number;
  public AUG: number;
  public SEP: number;
  public OKT: number;
  public NOV: number;
  public DEC: number;

  public toArray(): number[] {
    return [this.JAN, this.FEB, this.MAR, this.APR, this.MAY, this.JUN, this.JUL, this.AUG, this.SEP, this.OKT, this.NOV, this.DEC].slice();
  }
}
