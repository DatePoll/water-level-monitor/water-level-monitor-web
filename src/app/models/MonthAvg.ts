export class MonthAvg {
  public JAN: number;
  public FEB: number;
  public MAR: number;
  public APR: number;
  public MAY: number;
  public JUN: number;
  public JUL: number;
  public AUG: number;
  public SEP: number;
  public OKT: number;
  public NOV: number;
  public DEC: number;

  constructor(json: any) {
    this.JAN = json.JAN;
    this.FEB = json.FEB;
    this.MAR = json.MAR;
    this.APR = json.APR;
    this.MAY = json.MAY;
    this.JUN = json.JUN;
    this.JUL = json.JUL;
    this.AUG = json.AUG;
    this.SEP = json.SEP;
    this.OKT = json.OKT;
    this.NOV = json.NOV;
    this.DEC = json.DEC;
  }

  public formatCorrect(): number[] {
    return [this.JAN, this.FEB, this.MAR, this.APR, this.MAY, this.JUN, this.JUL, this.AUG, this.SEP, this.OKT, this.NOV, this.DEC];
  }
}
