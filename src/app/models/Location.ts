export class Location {
  public id: number;
  public name: string;
  public active: boolean;
}
